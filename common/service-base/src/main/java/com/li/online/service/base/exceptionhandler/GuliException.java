package com.li.online.service.base.exceptionhandler;

import com.li.online.service.base.result.ResultCodeEnum;
import lombok.Data;

/**
 * @author 11465
 */
@Data
public class GuliException extends RuntimeException{
    private ResultCodeEnum codeEnum; // 出现异常时 用来方便前端显示提示
    private Exception e; //以后出现异常时 用来分析异常代码

    public GuliException(ResultCodeEnum codeEnum){
        super(codeEnum.getMessage());
        this.codeEnum=codeEnum;
    }
    public GuliException(ResultCodeEnum codeEnum,Exception e){
        super(codeEnum.getMessage());
        this.codeEnum=codeEnum;
    }
}
