package com.li.online.service.base.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.logging.FileHandler;

/**
 * @author 11465
 * @Accessors(chain = true) 启用链式调用 get,set方法都带个BaseEntity返回值
 * @Accessors用于配置getter和setter方法的生成结果,chain设置为true，则setter方法返回当前对象
 */
@Data
@Accessors(chain = true)
public class BaseEntity {

    private String id;
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
