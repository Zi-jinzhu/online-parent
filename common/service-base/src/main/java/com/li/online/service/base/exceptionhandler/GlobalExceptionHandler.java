package com.li.online.service.base.exceptionhandler;

import com.li.online.service.base.result.R;
import com.li.online.service.base.result.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 11465
 * @RestControllerAdvice 异常处理组件类
 * @Slf4j
 * 日志筛选持久化保存：
 *  在service-edu 下的resources下创建logback-spring.xml配置文件，指定日志配置(文件名必须正确)，
 *  logback的日志配置激活需要在yaml文件中配置 spring.profiles.active=dev
 */
@Slf4j
@RestControllerAdvice//返回失败的R对象josn
public class GlobalExceptionHandler {


    @ExceptionHandler(value = GuliException.class)
    public R error(GuliException e){
        if(e.getE()!=null){
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return R.setResult(e.getCodeEnum());
    }



    /**
     * @ExceptionHandler(Exception.class) 注解标注过的方法都是异常处理器，处理注解绑定的方法
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public R error(Exception e){
//        System.out.println(ExceptionUtils.getStackTrace(e));
        log.error(ExceptionUtils.getStackTrace(e));
        return R.error();
    }

    /**
     * 特定异常 1/0
     * @param e
     * @return
     */
    @ExceptionHandler(value = ArithmeticException.class)
    public R error(ArithmeticException e){
//        System.out.println(ExceptionUtils.getStackTrace(e));
        log.error(ExceptionUtils.getStackTrace(e));
        return R.setResult(ResultCodeEnum.DIVISOR_CANNOT_ZERO);
    }

    /**
     * json格式异常
     * @param e
     * @return
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public R error(HttpMessageNotReadableException e){
        e.printStackTrace();
//        System.out.println(ExceptionUtils.getStackTrace(e));
        log.error(ExceptionUtils.getStackTrace(e));
        return R.setResult(ResultCodeEnum.JSON_PARSE_ERROR);
    }

}
