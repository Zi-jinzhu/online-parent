package com.li.online.service.edu.mapper;

import com.li.online.service.edu.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
