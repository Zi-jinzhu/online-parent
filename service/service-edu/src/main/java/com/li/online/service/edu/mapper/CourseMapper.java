package com.li.online.service.edu.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.li.online.service.edu.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.li.online.service.edu.entity.vo.CourseInfoVo;

import java.util.List;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface CourseMapper extends BaseMapper<Course> {

    List<CourseInfoVo> getCourseInfoVo(Page<CourseInfoVo> page);
}
