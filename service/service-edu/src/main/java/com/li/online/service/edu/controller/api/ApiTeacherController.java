package com.li.online.service.edu.controller.api;


import com.li.online.service.base.result.R;
import com.li.online.service.base.result.ResultCodeEnum;
import com.li.online.service.edu.entity.Teacher;
import com.li.online.service.edu.service.ServiceOssFeignClient;
import com.li.online.service.edu.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author li
 * @since 2021-10-06
 * @RestController相当于Controller加Responsebody
 */
@RestController
@RequestMapping("/api/edu/teacher")
public class ApiTeacherController {

    @Autowired
    TeacherService teacherService;

    /**
     * 根据讲师Id查询
     * @param id
     * @return
     */
    @GetMapping("getById/{id}")
    public R getTeacherById(@PathVariable String id){
        try {
            Teacher teacher = teacherService.getById(id);
            return R.ok().data("items",teacher);
        } catch (Exception e) {
            return R.error().message("除数不能为0");
        }
    }

    @Autowired
    ServiceOssFeignClient serviceOssFeignClient;
    /**
     * 查询所有讲师
     * @return
     */
    @GetMapping("list")
    public R list(){
        List<Teacher> list = teacherService.list();
        R r = serviceOssFeignClient.test(R.setResult(ResultCodeEnum.LOGIN_ERROR));
        System.out.println("r = " + r);
        return R.ok().data("items",list);
    }

}

