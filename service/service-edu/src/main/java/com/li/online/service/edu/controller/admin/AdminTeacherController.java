package com.li.online.service.edu.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.li.online.service.base.result.R;
import com.li.online.service.edu.entity.Teacher;
import com.li.online.service.edu.entity.vo.TeacherQuery;
import com.li.online.service.edu.service.ServiceOssFeignClient;
import com.li.online.service.edu.service.TeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author li
 * @since 2021-10-06
 * @RestController相当于Controller加Responsebody
 */
@Slf4j
@CrossOrigin//允许跨域，也可以加到具体方法上
@RestController
@RequestMapping("/admin/edu/teacher")
@Api(tags = "讲师管理模块")//对controller进行描述
public class AdminTeacherController {

    @Autowired
    TeacherService teacherService;

    /*
        400:代表请求参数错误(后端要求的参数没有传 或者传入的格式错误)

        后端处理请求时如果给前端统一响应，可以提高前端给用户提示的友好

        以后后端无论成功还是失败 都返回一个统一格式的数据，方便前端解析：
            R{
                Integer code;//状态码  20000代表成功  其它失败
                String message;//响应描述信息
                Boolean success;//是否成功
                Map<String,Object> data; //响应数据
            }
     */
    //http协议：报文由四部分组成(请求首行、请求头、请求空行、请求体)  get方式没有请求体所以不能使用RequestBody获取数据
    //1、pojo入参
    //public R updateById(Teacher teacher){ http://localhost:8110/admin/edu/teacher/updateById?id=1&name=xx&intro=xxx
    //2、RequestBody 将请求体中的json获取转为绑定的类型的对象(get以外的其它方式才可以使用)
    //3、@PathVariable  路径参数


    /**
     * 4.2,批量删除！！！
     * @param ids
     * @return
     */
    @DeleteMapping("batchDel")
    public R batchDel(@RequestBody List<String> ids){
        teacherService.removeByIds(ids);
        return R.ok();
    }


    /**
     * 5.3,讲师分页条件查询POST
     * @param pageNum
     * @param pageSize
     * @param teacherQuery @RequestBody(required = false)required = false表示绑定类的属性可以为空
     * @return
     * if (!StringUtils.isEmpty(name))不为null或空时加入条件
     */
    @PostMapping("pageTeacherCondition/{pageNum}/{pageSize}")
    @ApiOperation("讲师分页条件查询")
    public R pageTeacherCondition(@ApiParam(value = "页码",defaultValue = "1") @PathVariable Integer pageNum,
                                  @ApiParam(value = "每页记录条数",defaultValue = "3") @PathVariable Integer pageSize,
                                  @RequestBody(required = false) TeacherQuery teacherQuery){
        //创建page对象
        IPage<Teacher> page = new Page<>(pageNum,pageSize);
        teacherService.pageByCondition(teacherQuery,page);
        return R.ok().data("page",page);
    }

    /**
     * 5,前端给用这个!!!!!
     * @param pageNum
     * @param pageSize
     * @param teacherQuery
     * @return
     */
    @GetMapping("pageList/{pageNum}/{pageSize}")
    @ApiOperation("讲师分页条件查询")
    public R pageList(@ApiParam(value = "页码",defaultValue = "1") @PathVariable Integer pageNum,
                      @ApiParam(value = "每页记录条数",defaultValue = "3") @PathVariable Integer pageSize,
                      TeacherQuery teacherQuery){
        //创建page对象
        IPage<Teacher> page = new Page<>(pageNum,pageSize);
        teacherService.pageByCondition(teacherQuery,page);
        return R.ok().data("page",page);
    }

    /**
     * 5.2,分页
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("list/{pageNum}/{pageSize}")
    @ApiOperation("讲师分页查询")
    public R list(@ApiParam(value = "页码",defaultValue = "1") @PathVariable Integer pageNum,
                  @ApiParam(value = "每页记录条数",defaultValue = "3") @PathVariable Integer pageSize){
        IPage<Teacher> page = new Page<>(pageNum,pageSize);
        teacherService.page(page);
        return R.ok().data("page",page);
    }

    @Autowired
    ServiceOssFeignClient serviceOssFeignClient;
    /**
     * 4,删除
     * 删除讲师时还需删除oss讲中讲师头像，使用微服务远程访问
     * 注册中心使用nacos熔断使用sentinel
     * @param id
     * @return
     */
    @DeleteMapping("deleteById/{id}")
    @ApiOperation("讲师根据id删除")
    public R deleteById(@PathVariable String id){
        Teacher teacher = teacherService.getById(id);
        boolean b = teacherService.removeById(id);

        if (b) {
            if (!StringUtils.isEmpty(teacher.getAvatar())){
                serviceOssFeignClient.delete(teacher.getAvatar(),"avatar");
            }
            return R.ok();
        }
        return R.error().message("记录不存在");
    }

    /**
     * 3,新增
     * @param teacher
     * @return
     */
    @PostMapping("save")
    @ApiOperation("讲师新增")
    public R save(@RequestBody Teacher teacher){
        boolean save = teacherService.save(teacher);
        if (save) {
            return R.ok();
        }
        return R.error().message("新增失败");
    }

    /**
     * 2,更新
     * @param teacher
     * @return
     */
    @PutMapping("updateById")
    @ApiOperation("讲师更新")
    public R updateById(@RequestBody Teacher teacher){
        boolean b = teacherService.updateById(teacher);
        if (b) {
            return R.ok();
        }
        return R.error().message("记录不存在");
    }


    /**
     * 1.2,根据讲师Id查询
     * @param id
     * @return
     */
    @GetMapping("getById/{id}")
    @ApiOperation("根据讲师Id查询讲师")
    public R getTeacherById(@PathVariable String id){
        try {
            Teacher teacher = teacherService.getById(id);
            return R.ok().data("item",teacher);
        } catch (Exception e) {
            return R.error().message("除数不能为0");
        }
    }

    /**
     * 1,查询所有讲师
     * @return
     */
    @GetMapping("list")
    @ApiOperation("查询所有讲师")
    public R list(){
        log.debug("当前日志级别：{}，当前时间：{}","debug",new Date());
        //开发一般输出info级别
        log.info("当前日志级别：{}，当前时间：{}","info",new Date());
        //生成环一般是warn和error
        log.warn("当前日志级别：{}，当前时间：{}","warn",new Date());
        log.error("当前日志级别：{}，当前时间：{}","error",new Date());
        List<Teacher> list = teacherService.list();
        return R.ok().data("items",list);
    }

}

