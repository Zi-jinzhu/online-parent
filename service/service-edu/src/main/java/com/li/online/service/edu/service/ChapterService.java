package com.li.online.service.edu.service;

import com.li.online.service.edu.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface ChapterService extends IService<Chapter> {

}
