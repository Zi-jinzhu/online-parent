package com.li.online.service.edu.service.impl;

import com.li.online.service.edu.entity.CourseDescription;
import com.li.online.service.edu.mapper.CourseDescriptionMapper;
import com.li.online.service.edu.service.CourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Service
public class CourseDescriptionServiceImpl extends ServiceImpl<CourseDescriptionMapper, CourseDescription> implements CourseDescriptionService {

}
