package com.li.online.service.edu.service.impl;

import com.li.online.service.edu.entity.Video;
import com.li.online.service.edu.mapper.VideoMapper;
import com.li.online.service.edu.service.VideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Service
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements VideoService {

}
