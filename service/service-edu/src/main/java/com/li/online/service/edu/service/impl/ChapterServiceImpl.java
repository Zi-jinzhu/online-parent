package com.li.online.service.edu.service.impl;

import com.li.online.service.edu.entity.Chapter;
import com.li.online.service.edu.mapper.ChapterMapper;
import com.li.online.service.edu.service.ChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements ChapterService {

}
