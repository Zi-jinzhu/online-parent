package com.li.online.service.edu.service.impl;

import com.li.online.service.edu.entity.CourseCollect;
import com.li.online.service.edu.mapper.CourseCollectMapper;
import com.li.online.service.edu.service.CourseCollectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程收藏 服务实现类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Service
public class CourseCollectServiceImpl extends ServiceImpl<CourseCollectMapper, CourseCollect> implements CourseCollectService {

}
