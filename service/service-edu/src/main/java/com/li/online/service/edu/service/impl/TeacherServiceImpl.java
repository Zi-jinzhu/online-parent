package com.li.online.service.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.li.online.service.edu.entity.Teacher;
import com.li.online.service.edu.entity.vo.TeacherQuery;
import com.li.online.service.edu.mapper.TeacherMapper;
import com.li.online.service.edu.service.TeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

    /**
     *
     * @param teacherQuery
     * @param pageTeacher
     * this.page(pageTeacher,wrapper);
     * this是当前类因为实例化了IService接口可以使用其方法，与在controller层使用效果一样，
     * page是方法名别看错了，
     * 查询结果会自动放在pageTeacher，也就是controller层的page对象
     */
    @Override
    public void pageByCondition(TeacherQuery teacherQuery, IPage<Teacher> pageTeacher) {
        //构建条件
        QueryWrapper<Teacher> wrapper = new QueryWrapper<>();

        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String joinDateBegin = teacherQuery.getJoinDateBegin();
        String joinDateEnd = teacherQuery.getJoinDateEnd();
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name",name);
        }
        if (!StringUtils.isEmpty(level)) {
            wrapper.eq("level",level);
        }
        if (!StringUtils.isEmpty(joinDateBegin)) {
            wrapper.ge("join_date",joinDateBegin);
        }
        if (!StringUtils.isEmpty(joinDateEnd)) {
            wrapper.le("join_date",joinDateEnd);
        }

        this.page(pageTeacher,wrapper);
    }
}
