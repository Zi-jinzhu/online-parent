package com.li.online.service.edu.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.li.online.service.edu.entity.Course;
import com.li.online.service.edu.entity.CourseDescription;
import com.li.online.service.edu.entity.form.CourseInfoForm;
import com.li.online.service.edu.entity.vo.CourseInfoVo;
import com.li.online.service.edu.mapper.CourseDescriptionMapper;
import com.li.online.service.edu.mapper.CourseMapper;
import com.li.online.service.edu.service.CourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Service
@Transactional//当前业务类的所有方法都被事务管理
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

    @Autowired
    CourseDescriptionMapper courseDescriptionMapper;
    @Override
    public String saveCourseInfo(CourseInfoForm courseInfoForm) {
        Course course = new Course();
        BeanUtils.copyProperties(courseInfoForm,course);
        baseMapper.insert(course);
        CourseDescription courseDescription = new CourseDescription();
        courseDescription.setId(course.getId());
        courseDescription.setDescription(courseInfoForm.getDescription());
        courseDescriptionMapper.insert(courseDescription);
        return course.getId();
    }

    @Override
    public CourseInfoForm getCourseInfoById(String id) {
        Course course = baseMapper.selectById(id);
        CourseDescription courseDescription = courseDescriptionMapper.selectById(id);
        CourseInfoForm courseInfoForm = new CourseInfoForm();
        BeanUtils.copyProperties(course,courseInfoForm);
        courseInfoForm.setDescription(courseDescription.getDescription());
        return courseInfoForm;
    }

    /**
     * 注意CourseInfoForm类里没有id字段
     * @param id
     * @param courseInfoForm
     */
    @Override
    public void updateCourseInfo(String id, CourseInfoForm courseInfoForm) {
        Course course = new Course();
        BeanUtils.copyProperties(courseInfoForm,course);
        course.setId(id);
        baseMapper.updateById(course);
        CourseDescription courseDescription = new CourseDescription();
        courseDescription.setId(id);
        courseDescription.setDescription(courseInfoForm.getDescription());
        courseDescriptionMapper.updateById(courseDescription);


    }

    @Override
    public void getCourseInfoVo(Page<CourseInfoVo> page) {
        List<CourseInfoVo> courseInfoVos = baseMapper.getCourseInfoVo(page);
        page.setRecords(courseInfoVos);
    }
}
