package com.li.online.service.edu.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author 11465
 * 此类只是对应excel表的表中有啥就写啥数据库中的字段由我们自己写，
 * 在读取每一条时就加入其他字段
 */
@Data
public class SubjectExcelData {
    @ExcelProperty(value = "一级分类")
    private String levelOneTitle;
    @ExcelProperty(value = "二级分类")
    private String levelTwoTitle;
}
