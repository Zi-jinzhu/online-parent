package com.li.online.service.edu.controller.admin;


import com.li.online.service.base.result.R;
import com.li.online.service.edu.entity.Subject;
import com.li.online.service.edu.service.SubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Slf4j
@CrossOrigin//允许跨域，也可以加到具体方法上
@RestController
@RequestMapping("/admin/edu/subject")
@Api(tags = "课程类目模块")//对controller进行描述
public class AdminSubjectController {
    @Autowired
    SubjectService subjectService;

    //2、查询课程分类一级和二级嵌套集合
    @ApiOperation("查询课程分类一级和二级嵌套集合")
    @GetMapping("getNestedSubject")
    public R getNestedSubject(){
        List<Subject> pSubject = subjectService.getNestedSubject();
        return R.ok().data("items",pSubject);

    }


    //1、导入课程分类
    // 接收上传的excel文件 使用easyexcel解析数据并存到 数据库edu_subject表中
    @ApiOperation("导入课程分类")
    @PostMapping("import")
    public R importSubject(MultipartFile file){
        subjectService.importSubject(file);
        return R.ok();

    }
}

