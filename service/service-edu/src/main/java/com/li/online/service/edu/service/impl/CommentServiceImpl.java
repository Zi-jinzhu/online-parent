package com.li.online.service.edu.service.impl;

import com.li.online.service.edu.entity.Comment;
import com.li.online.service.edu.mapper.CommentMapper;
import com.li.online.service.edu.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

}
