package com.li.online.service.edu.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.li.online.service.edu.entity.Subject;
import com.li.online.service.edu.entity.excel.SubjectExcelData;
import com.li.online.service.edu.mapper.SubjectMapper;

/**
 * @author 11465
 * 此类为被容器管理
 *      第一种加@Service
 *      第二种带参构造，SubjectServiceImpl自定义的泛型里有
 *
 */
//@Service
public class SubjectDataListener extends AnalysisEventListener<SubjectExcelData> {

    //@Autowired //只能在组件类中使用，从管理组件类的容器中 自动装配获取对象
    SubjectMapper subjectMapper;
    public SubjectDataListener(SubjectMapper subjectMapper){
        this.subjectMapper=subjectMapper;
    }

    //SubjectExcelData:一个对象包含 两个课程分类标题数据

    /**
     *
     * @param subjectExcelData
     * @param analysisContext
     */
    @Override
    public void invoke(SubjectExcelData subjectExcelData, AnalysisContext analysisContext) {
        //每读取一行数据 保存到数据库一次
        String levelOneTitle = subjectExcelData.getLevelOneTitle();
        String levelTwoTitle = subjectExcelData.getLevelTwoTitle();
        //初始化一级课程分类对象存到数据库
        //1、先检查一级课程分类是否存在
        LambdaQueryWrapper<Subject> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Subject::getTitle,levelOneTitle);
        wrapper.eq(Subject::getParentId,"0");
        Subject levelOneSubject = subjectMapper.selectOne(wrapper);
        //2、如果不存在再保存
        if(levelOneSubject==null){
            levelOneSubject = new Subject();
            levelOneSubject.setTitle(levelOneTitle);
            levelOneSubject.setParentId("0");//0代表一级分类
            levelOneSubject.setSort(0);
            subjectMapper.insert(levelOneSubject);
        }
        // 当前位置 levelOneSubject要么从数据库查询到  要么是自己初始化并存到数据库
        //初始化二级课程分类对象存到数据库
        //3、先检查二级分类是否存在
        wrapper.clear();//清空缓存的条件
        wrapper.eq(Subject::getTitle,levelTwoTitle);
        wrapper.eq(Subject::getParentId,levelOneSubject.getId());
        if(subjectMapper.selectCount(wrapper)==0){
            //4、二级分类不存在 新增
            Subject levelTwoSubject = new Subject();
            levelTwoSubject.setTitle(levelTwoTitle);
            levelTwoSubject.setParentId(levelOneSubject.getId());
            levelTwoSubject.setSort(0);
            subjectMapper.insert(levelTwoSubject);
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
