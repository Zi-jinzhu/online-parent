package com.li.online.service.edu.mapper;

import com.li.online.service.edu.entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

}
