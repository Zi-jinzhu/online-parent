package com.li.online.service.edu.service;

import com.li.online.service.edu.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface SubjectService extends IService<Subject> {

    void importSubject(MultipartFile file);

    List<Subject> getNestedSubject();

}
