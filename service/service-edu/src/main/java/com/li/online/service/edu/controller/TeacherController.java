package com.li.online.service.edu.controller;


import com.li.online.service.base.result.R;
import com.li.online.service.edu.entity.Teacher;
import com.li.online.service.edu.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author li
 * @since 2021-10-06
 * @RestController相当于Controller加Responsebody
 */
@RestController
@RequestMapping("/edu/teacher")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    /*
        400:代表请求参数错误(后端要求的参数没有传 或者传入的格式错误)

        后端处理请求时如果给前端统一响应，可以提高前端给用户提示的友好

        以后后端无论成功还是失败 都返回一个统一格式的数据，方便前端解析：
            R{
                Integer code;//状态码  20000代表成功  其它失败
                String message;//响应描述信息
                Boolean success;//是否成功
                Map<String,Object> data; //响应数据
            }
     */

    /**
     * 根据讲师Id查询
     * @param id
     * @return
     */
    @GetMapping("getById/{id}")
    public R getTeacherById(@PathVariable String id){
        try {
            Teacher teacher = teacherService.getById(id);
            return R.ok().data("items",teacher);
        } catch (Exception e) {
            return R.error().message("除数不能为0");
        }
    }

    /**
     * 查询所有讲师
     * @return
     */
    @GetMapping("list")
    public R list(){
        List<Teacher> list = teacherService.list();
        return R.ok().data("items",list);
    }

}

