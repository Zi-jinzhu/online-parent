package com.li.online.service.edu.mapper;

import com.li.online.service.edu.entity.CourseCollect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程收藏 Mapper 接口
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface CourseCollectMapper extends BaseMapper<CourseCollect> {

}
