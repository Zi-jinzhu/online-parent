package com.li.online.service.edu.controller.admin;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.li.online.service.base.result.R;
import com.li.online.service.edu.entity.Course;
import com.li.online.service.edu.entity.CourseDescription;
import com.li.online.service.edu.entity.form.CourseInfoForm;
import com.li.online.service.edu.entity.vo.CourseInfoVo;
import com.li.online.service.edu.service.CourseDescriptionService;
import com.li.online.service.edu.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
@Slf4j
@CrossOrigin//允许跨域，也可以加到具体方法上
@RestController
@RequestMapping("/admin/edu/course")
@Api(tags = "课程管理模块")
public class AdminCourseController {
    @Autowired
    CourseService courseService;

    /**
     * 如果自定义方法传入page对象，mp的分页过滤器会自动使用page对象拼接条件
     *      limit会自动加
     * @param pageNum
     * @param pageSize
     * @return
     */
    @ApiOperation("分页查询课程列表")
    @GetMapping("list/{pageNum}/{pageSize}")
    public R list(@PathVariable Integer pageNum,@PathVariable Integer pageSize){
        Page<CourseInfoVo> page = new Page<>(pageNum,pageSize);
        courseService.getCourseInfoVo(page);
        return R.ok().data("page",page);
    }

    @ApiOperation("更新课程")
    @PutMapping("update-CourseInfo/{id}")
    public R updateCourseInfo(@PathVariable String id,@RequestBody CourseInfoForm courseInfoForm){
        courseService.updateCourseInfo(id,courseInfoForm);
        return R.ok();
    }

    @ApiOperation("根据Id查找课程")
    @GetMapping("get-CourseInfo/{id}")
    public R getCourseInfoById(@PathVariable String id) {
        CourseInfoForm courseInfoForm = courseService.getCourseInfoById(id);
        return R.ok().data("item", courseInfoForm);
    }

    @ApiOperation("保存课程信息")
    @PostMapping("save-CourseInfo")
    public R saveCourseInfo(@RequestBody CourseInfoForm courseInfoForm) {
        String courseId = courseService.saveCourseInfo(courseInfoForm);
        return R.ok().data("id", courseId);
    }

}

