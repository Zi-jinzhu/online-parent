package com.li.online.service.edu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.li.online.service.edu.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.li.online.service.edu.entity.form.CourseInfoForm;
import com.li.online.service.edu.entity.vo.CourseInfoVo;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface CourseService extends IService<Course> {

    String saveCourseInfo(CourseInfoForm courseInfoForm);

    CourseInfoForm getCourseInfoById(String id);


    void updateCourseInfo(String id, CourseInfoForm courseInfoForm);

    void getCourseInfoVo(Page<CourseInfoVo> page);
}
