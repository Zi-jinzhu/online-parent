package com.li.online.service.edu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.li.online.service.edu.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.li.online.service.edu.entity.vo.TeacherQuery;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface TeacherService extends IService<Teacher> {

    void pageByCondition(TeacherQuery teacherQuery, IPage<Teacher> pageTeacher);
}

