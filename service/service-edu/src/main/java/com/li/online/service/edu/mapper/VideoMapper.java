package com.li.online.service.edu.mapper;

import com.li.online.service.edu.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface VideoMapper extends BaseMapper<Video> {

}
