package com.li.online.service.edu.service;

import com.li.online.service.edu.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author li
 * @since 2021-10-06
 */
public interface CommentService extends IService<Comment> {

}
