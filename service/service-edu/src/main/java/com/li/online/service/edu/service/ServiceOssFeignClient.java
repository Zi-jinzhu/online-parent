package com.li.online.service.edu.service;

import com.li.online.service.base.result.R;
import com.li.online.service.edu.service.impl.ServiceOssFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author 11465
 * feign和ribbon底层都是用HttpClient实现远程访问的：
 *      HttpClient发送请求需要配置请求方式+请求地址+参数
 *              服务器地址
 *              接口路径
 *              接口方式
 *              参数
 */
//fallback指定兜底方案
@FeignClient(value = "service-oss",fallback = ServiceOssFeignClientFallback.class)// 通过service-oss可以在注册中心获取ip+端口号
public interface ServiceOssFeignClient {


    @DeleteMapping("/admin/oss/delete")
    public R delete(@RequestParam String path,@RequestParam String module);









    /*
        如果远程访问需要传参数必须要在参数前添加注解：
            1、请求参数
                @RequestParam
            2、请求体参数
              @RequestBody
            3、路径参数
              @PathVariable
     */
    // http://localhost:8120/admin/oss/test
    @PostMapping("/admin/oss/test")// GetMapping指定请求方式使用get方式
    //方法的返回值决定了远程访问成功后响应体内容要转换的类型
     R test(@RequestBody R r);//方法形参就是访问时的参数列表
}
