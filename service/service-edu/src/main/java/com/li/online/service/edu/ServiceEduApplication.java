package com.li.online.service.edu;

import feign.Logger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动类
 * @author 11465
 * @SpringBootApplication(exclude = DataSourceAutoConfiguration.class)连接数据库时取消参数exclude
 * 启动时需要添加数据库配置（因为service-base导入了mysql依赖）
 */
//@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableTransactionManagement//启用声明式事务管理
@SpringBootApplication
@EnableDiscoveryClient//启用客户端把自注册nacos，可加可不加
@EnableFeignClients//负责扫描@FeignClient注解创建对象注入到容器中。
@MapperScan(basePackages = "com.li.online.service.edu.mapper")
@ComponentScan(basePackages = "com.li.online.service")//扫描spring,springMVC的组件注解
public class ServiceEduApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceEduApplication.class,args);
    }

    @Bean
    public Logger.Level level(){
        return Logger.Level.FULL;
    }
}
