package com.li.online.service.edu.service.impl;

import com.li.online.service.base.result.R;
import com.li.online.service.edu.service.ServiceOssFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 11465
 */
@Service
@Slf4j
public class ServiceOssFeignClientFallback implements ServiceOssFeignClient {

    /**oss文件删除兜底方案
     * @param path
     * @param module
     * @return
     */
    @Override
    public R delete(String path, String module) {
        log.error("远程删除讲师头像失败：{}，{}"+path,module);
        return R.error();
    }

    /**
     * 兜底方法
     * @param r
     * @return
     */
    @Override
    public R test(R r) {
        log.info("远程访问失败");
        return R.error();
    }
}
