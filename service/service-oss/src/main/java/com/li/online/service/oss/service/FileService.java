package com.li.online.service.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author 11465
 */
public interface FileService {
    String upload(MultipartFile file,String module);

    void delete(String path,String module);
}
