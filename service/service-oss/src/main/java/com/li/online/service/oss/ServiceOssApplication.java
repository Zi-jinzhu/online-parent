package com.li.online.service.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 11465
 * 由于项目依赖了service-base，
 * service-base中引入了数据库相关依赖导致当前项目启动时自动配置会初始化数据库连接池，
 * 需要使用数据库连接配置参数
 *
 *
 * 解决：
 *      （排除数据库相关依赖）
 *      排除数据库连接池自动配置类 exclude = DataSourceAutoConfiguration.class
 */
@EnableDiscoveryClient//启用客户端把自注册nacos，可加可不加
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = "com.li.online.service")

public class ServiceOssApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceOssApplication.class,args);
    }
}
