package com.li.online.service.oss.controller;

import com.li.online.service.base.result.R;
import com.li.online.service.oss.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 11465
 */
@CrossOrigin//允许跨域，也可以加到具体方法上
@RestController
@Slf4j
@RequestMapping("/admin/oss")
public class FileController {

    @Autowired
    FileService fileService;

    @PostMapping("test")
    public R test(@RequestBody R r) throws InterruptedException {
        log.info("接收参数为：{}，{}"+r);
        Thread.sleep(3000);
        return R.ok();
    }

    @PostMapping("upload")
    public R upload(MultipartFile file,String module){//文件上传表单项的name值必须为file
        String imgUrl = fileService.upload(file,module);
        return R.ok().data("path",imgUrl);
    }

    /**
     * oss文件删除
     * @return
     */
    @DeleteMapping("delete")
    public R delete(String path,String module){
        fileService.delete(path,module);
        return R.ok();
    }
}
